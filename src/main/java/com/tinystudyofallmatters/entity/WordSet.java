package com.tinystudyofallmatters.entity;

import java.util.List;

public record WordSet(int id, String category, String word, List<String> forbiddenWords) {
}
