package com.tinystudyofallmatters.service;

import com.tinystudyofallmatters.entity.WordSet;
import org.json.JSONArray;
import org.json.JSONObject;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class WordService {
    private final WordConnector wordConnector;

    public WordService(WordConnector wordConnector) {
        this.wordConnector = wordConnector;
    }

    public List<String> listCategories() throws IOException {
        JSONArray categories = wordConnector.getCategories();
        List<String> listCategories = new ArrayList<>();
        for (int i = 0; i < categories.length(); i++) {
            listCategories.add(categories.getString(i));
        }
        return listCategories;
    }

    public List<WordSet> listWords(String category, int count) throws IOException {
        JSONArray words = wordConnector.getWords(category, count);
        List<WordSet> listWords = new ArrayList<>();
        for (int i = 0; i < words.length(); i++) {
            JSONObject jsonObject = words.getJSONObject(i);
            int id = jsonObject.getInt("id");
            String word = jsonObject.getString("mainWord");
            JSONArray jsonArray = jsonObject.getJSONArray("forbiddenWords");
            List<String> listForbiddenWords = new ArrayList<>();
            for (int j = 0; j < jsonArray.length(); j++) {
                listForbiddenWords.add(jsonArray.getString(j));
            }
            WordSet wordSet = new WordSet(id, category, word, listForbiddenWords);
            listWords.add(wordSet);
        }
        return listWords;
    }
}
