package com.tinystudyofallmatters.service;

import com.tinystudyofallmatters.entity.Player;
import com.tinystudyofallmatters.entity.WordSet;

import java.io.IOException;
import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class ConsoleService {
    private final Scanner scanner = new Scanner(System.in);
    private WordService wordService;

    public ConsoleService(WordService wordService) {
        this.wordService = wordService;
    }

    public int getCountWords() {
        System.out.println("Type how many words you want per player: ");
        int answer = scanner.nextInt();
        scanner.nextLine();
        return answer;
    }

    public List<Player> createPlayers() {
        System.out.println("Type amount of players: ");
        int amount = scanner.nextInt();
        scanner.nextLine();
        List<Player> playerList = new ArrayList<>();
        for (int i = 0; i < amount; i++) {
            System.out.println("Type name of player: ");
            Player player = new Player(scanner.nextLine());
            playerList.add(player);
        }
        return playerList;
    }

    public String chooseCategories(List<String> categories) {
        while (true) {
            System.out.println("Available categories: ");
            categories.forEach(System.out::println);
            System.out.println("Type category: ");
            String category = scanner.nextLine();
            if (categories.contains(category)) {
                return category;
            }
        }
    }

    public void printWord(WordSet wordSet, List<Player> players, Player currentPlayer) throws IOException {
        List<Player> listWithoutCurrentPlayer = players.stream()
                .filter(Predicate.not(currentPlayer::equals))
                .toList();

        while (true) {
            System.out.println(wordSet.word());
            System.out.println(wordSet.forbiddenWords());
            System.out.println("Who guessed? ");
            for (int i = 1; i < listWithoutCurrentPlayer.size() + 1; i++) {
                System.out.println(i + ") " + listWithoutCurrentPlayer.get(i - 1).getName());
            }
            System.out.println(listWithoutCurrentPlayer.size() + 1 + ") " + "Want to change the word. ");
            System.out.println(listWithoutCurrentPlayer.size() + 2 + ") " + "We give up. ");
            System.out.println("Type the number: ");
            int answer = scanner.nextInt();
            scanner.nextLine();
            if (answer == listWithoutCurrentPlayer.size() + 1) {
                this.printWord(wordService.listWords(wordSet.category(), 1).get(0), listWithoutCurrentPlayer, currentPlayer);
                return;
            } else if (answer == listWithoutCurrentPlayer.size() + 2) {
                return;
            } else if (answer < 1) {
                System.out.println("To low number. ");
            } else if (answer > listWithoutCurrentPlayer.size() + 2) {
                System.out.println("To big number. ");
            } else {
                Player player = listWithoutCurrentPlayer.get(answer - 1);
                player.setScore(player.getScore() + 1);
                return;
            }
        }
    }

    public void printWinner(List<Player> players) {
        players.stream()
                .collect(Collectors.groupingBy(Player::getScore, TreeMap::new, Collectors.toList()))
                .lastEntry()
                .getValue().stream()
                .map(Player::getName)
                .forEach(x -> System.out.println("The winner is: " + x));

    }

    public void printScore(List<Player> players) {
        for (int i = 0; i < players.size(); i++) {
            System.out.println(players.get(i).getName() + ": " + players.get(i).getScore() + " points");
        }
    }

    public boolean wantToPlay() {
        while (true) {
            System.out.println("Do you want to continue game? ");
            System.out.println("1 - yes");
            System.out.println("2 - no");
            int answer = scanner.nextInt();
            scanner.nextLine();
            if (answer == 1) {
                return true;
            } else if (answer == 2) {
                return false;
            }
        }
    }
}
