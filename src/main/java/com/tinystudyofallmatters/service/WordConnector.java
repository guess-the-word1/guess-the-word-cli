package com.tinystudyofallmatters.service;

import org.json.JSONArray;
import org.json.JSONTokener;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

public class WordConnector {
    private static final String HOST = "https://guesstheword.andret.eu";

    public JSONArray getCategories() throws IOException {
        URL url = new URL(HOST + "/category");
        HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
        org.json.JSONTokener jsonTokener = new JSONTokener(httpURLConnection.getInputStream());
        return new JSONArray(jsonTokener);
    }

    public JSONArray getWords(String category, int count) throws IOException {
        URL url = new URL(HOST + "/" + category + "/" + count);
        HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
        org.json.JSONTokener jsonTokener = new JSONTokener(httpURLConnection.getInputStream());
        return new JSONArray(jsonTokener);
    }


}
