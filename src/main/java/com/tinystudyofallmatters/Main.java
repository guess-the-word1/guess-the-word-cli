package com.tinystudyofallmatters;

import com.tinystudyofallmatters.entity.Player;
import com.tinystudyofallmatters.entity.WordSet;
import com.tinystudyofallmatters.service.ConsoleService;
import com.tinystudyofallmatters.service.WordConnector;
import com.tinystudyofallmatters.service.WordService;

import java.io.IOException;
import java.util.List;
import java.util.Scanner;

public class Main {
    private static final Scanner scanner = new Scanner(System.in);
    public static void main(String[] args) throws IOException {
        WordConnector wordConnector = new WordConnector();
        WordService wordService = new WordService(wordConnector);
        ConsoleService consoleService = new ConsoleService(wordService);


        List<Player> players = consoleService.createPlayers();
        int countWords = consoleService.getCountWords();


        do {
            players.forEach(currentPlayer -> {
                try {
                    System.out.println("Give the pc to " + currentPlayer.getName());
                    System.out.println("Press enter to continue. ");
                    scanner.nextLine();
                    String category = consoleService.chooseCategories(wordService.listCategories());
                    List<WordSet> wordSets = wordService.listWords(category, countWords);
                    for (int i = 0; i < wordSets.size(); i++) {
                        consoleService.printWord(wordSets.get(i), players, currentPlayer);
                    }
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            });
            consoleService.printScore(players);

        } while (consoleService.wantToPlay());

        consoleService.printWinner(players);
    }
}
