package com.tinystudyofallmatters.service;


import com.tinystudyofallmatters.entity.WordSet;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.jupiter.api.Test;


import java.io.IOException;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class WordServiceTest {
    @Test
    void listCategories() throws IOException {
        // given
        WordConnector wordConnector = mock(WordConnector.class);
        String category1 = "Fantasy";
        String category2 = "Medival";
        when(wordConnector.getCategories()).thenReturn(new JSONArray().put(category1).put(category2));
        // when
        WordService wordService = new WordService(wordConnector);
        List<String> categories = wordService.listCategories();
        // then
        assertThat(categories).contains(category1, category2);
    }

    @Test
    void listEmptyCategories() throws IOException {
        // given
        WordConnector wordConnector = mock(WordConnector.class);
        when(wordConnector.getCategories()).thenReturn(new JSONArray());
        // when
        WordService wordService = new WordService(wordConnector);
        List<String> categories = wordService.listCategories();
        // then
        assertThat(categories).isEmpty();
    }

    @Test
    void listWords() throws IOException {
        // given
        WordConnector wordConnector = mock(WordConnector.class);
        when(wordConnector.getWords(eq("Fantasy"), anyInt())).thenReturn(
                new JSONArray()
                        .put(new JSONObject()
                                .put("id", 1)
                                .put("category", "Fantasy")
                                .put("mainWord", "Harry Potter")
                                .put("forbiddenWords", new JSONArray()
                                        .put("")
                                        .put(""))));
        // when
        WordService wordService = new WordService(wordConnector);
        List<WordSet> wordSets = wordService.listWords("Fantasy", 5);
        // then
        assertThat(wordSets).contains(new WordSet(1, "Fantasy", "Harry Potter", List.of("", "")));
    }

    @Test
    void listMoreWords() throws IOException {
        // given
        WordConnector wordConnector = mock(WordConnector.class);
        when(wordConnector.getWords(eq("Fantasy"), anyInt())).thenReturn(
                new JSONArray()
                        .put(new JSONObject()
                                .put("id", 1)
                                .put("category", "Fantasy")
                                .put("mainWord", "Harry Potter")
                                .put("forbiddenWords", new JSONArray()
                                        .put("")
                                        .put("")))
                        .put(new JSONObject()
                                .put("id", 2)
                                .put("category", "Fantasy")
                                .put("mainWord", "Aragorn")
                                .put("forbiddenWords", new JSONArray()
                                        .put("")
                                        .put("")))
                        .put(new JSONObject()
                                .put("id", 3)
                                .put("category", "Fantasy")
                                .put("mainWord", "Vader")
                                .put("forbiddenWords", new JSONArray()
                                        .put("")
                                        .put(""))));
        // when
        WordService wordService = new WordService(wordConnector);
        List<WordSet> wordSets = wordService.listWords("Fantasy", 5);
        // then
        assertThat(wordSets).contains(
                new WordSet(1, "Fantasy", "Harry Potter", List.of("", "")),
                new WordSet(2, "Fantasy", "Aragorn", List.of("", "")),
                new WordSet(3, "Fantasy", "Vader", List.of("", "")));
    }

}
