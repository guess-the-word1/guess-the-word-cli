# Guess The Word CLI

#### Technologies: Java, Spring, RESTful API, JSON, asynchronous queries
#### Functionalities:
- API with a database of words prepared by me.
- Execution of asynchronous queries on the client side to my exposed API.
- Setting the number of players.
- Creating a player profile, saving the name.
- Adjusting the round length.
- Displaying and the option to choose word categories and mixed options.
- Saving results and displaying a scoreboard after each round.
- Possibility for the team to surrender and move on to the next word - no one gets a point.
- Switching the active player. 


#### From the Client Side:
- The client is asked to enter the number of players.
- The client is asked to enter the names of the players.
- The client is asked to enter how many words each player should play.
- The client receives information about which player should pass the device.
- When the correct player has the device, they click next to confirm.
- The player is shown available word categories, including mixed options. The player chooses one of the options.
- The player is shown the main word that they should explain to other players. However, they cannot use the words displayed below. When one of the players guesses, the active player selects their name to score a point. The player has the option to surrender and move on to another word.
- The game repeats so that each player plays their words.
- After playing the rounds, the application shows the scoreboard and asks if the players want to continue.
